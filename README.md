# Stacked
Stacked is a simple, stack-based esoteric programming language. By no means is it intended to be functional or useful. It is written using an ANTLR4 grammar, which is then used to generate the Python 3 code for interacting with the parser.

- [Stacked](#stacked)
  - [Syntax](#syntax)
    - [Values and Operators](#values-and-operators)
      - [Avaliable Operators](#avaliable-operators)
    - [Comments](#comments)
    - [Conditionals](#conditionals)
    - [Functions](#functions)
      - [Anonymous Functions](#anonymous-functions)
      - [Named Functions](#named-functions)
      - [Advanced Function Example](#advanced-function-example)

## Syntax
### Values and Operators
The entire programming language is focussed around a stack (hence the name). *Values* can be pushed onto the stack, and *Operators* or *Functions* mutate and change those values. For example, let's consider a super simple program:

```
1 2 +
```

This program will result in a final stack of `[3]`. We can visualise the execution of the program using a table:

| Instruction | Stack                 |
| ----------- | --------------------- |
| **1** 2 +   | `[] -> [1]   `        |
| 1 **2** +   | `[1] -> [2,1]  `      |
| 1 2 **+**   | `[2,1] -> 2+1 -> [3]` |

The plus operator pops the top two items off the stack, adds them together, and then pushed them back onto the stack. Some more examples:

```
2 1 +
=> [3]

2 3 +
=> [5]

1 2 3 + +
=> [6]
```

#### Avaliable Operators
| Operator | Meaning                                                       | Stack Effect Diagram |
| -------- | ------------------------------------------------------------- | -------------------- |
| +        | **Add** the top two numbers on the stack.                     | a b -- a+b           |
| -        | **Subtract** the top two numbers on the stack.                | a b -- a-b           |
| *        | **Multiply** the top two numbers on the stack.                | a b -- a*b           |
| /        | **Divide** the top two numbers on the stack.                  | a b -- a/b           |
| ^        | **Exponentiate** the top two numbers on the stack.            | a b -- a^b           |
| dup      | **Duplicate** the number on the top of the stack.             | a -- a a             |
| swap     | **Swap** the top two numbers on the stack.                    | a b -- b a           |
| drop     | **Remove** the number on top of the stack.                    | a b -- b             |
| over     | **Duplicate & Move** the number on top  behind the next item. | a b -- a b a         |
| rot      | **Rotate** the next three items one cycle                     | a b c -- b c a       |


### Comments
Comments are of the form

```c
1 2 +
/* Hello, I am a comment. */
```

### Conditionals
In order to add an if-like functionality to the program, there must be some way of only doing something if a condition is true. In order to do this, there are three special instructions for controlling program flow:

- `neg(instruction)`: Only execute instruction if the top of the stack is negative.
- `pos(instruction)`: Only execute instruction if the top of the stack is positive (not including zero).
- `zer(instruction)`: Only execute instruction if the top of the stack is exactly zero.

Therefore, a program such as

```
5 5 * pos(10)
```

Will give `[10 25]` as a final stack, whereas

```
5 5 * neg(10)
```

Will remain as `[25]`. However, there is a problem. Consider the following code:

```
5 5 * pos(0) zer(10)
```

The writer of the program wants to add `0` to the stack if the top of the stack is positive, and add `10` to the top of the stack if the top of the stack is zero. However, when this program runs, the final stack looks like `[10 0 25]`, not `[0 25]` like intended.

This is because when the `pos()` condition is met, zero is then added onto the stack. This means that when the next `zer()` condition is checked, then it is true and the `10` is also added to the stack.

A solution to this problem requires a knowledge of functions.

### Functions
Functions come in two flavours: *anonymous* and *named*. Functions take no arguments, and execute their instructions directly on the stack, like so:

```
5 { dup * }
=> 25
```

The `{dup *}` code is executed, meaning 5 is duplicated and then multiplied by itself.

#### Anonymous Functions
Anonymous functions are useful for when you want to execute multiple instructions if a condition is met. For example, the following code is **NOT** valid:

```
5 5 * pos(10 10 *)
```

but this code **IS** valid:

```
5 5 * pos({ 10 10 * })
```

#### Named Functions
Named functions are useful for when you want a re-usable block of code. For example, let's take the `{ dup * }` function from earlier and turn it into a `_square_` function.

All functions have one underscore around their name. This means that you do not override a keyword when making a function.

```c
| _square_ | {
    dup *
}

5 _square_

=> [25]
```

#### Advanced Function Example
Let's say we want to write the factorial function. We could define it like so in Python:

```python
def factorial(n):
    if n == 1:
        return 1

    return n*factorial(n-1)
```

Here it is, defined in Stacked:

```c++
| _factorial_ | {
    dup 1 -

    zer(
        { 
            swap
            drop 1
            swap
        }
    )

    pos(
        {
            swap
            dup 1 -
            _factorial_
            *
            swap
        }
    )

    drop
}

10 _factorial_
```

The reason for the swaps inside the `zer` and `pos` conditionals is to conserve the original value when the conditional was checked. This means that when the next conditional is called, it recieves the same value as the first one.