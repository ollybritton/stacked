// Generated from /Users/olly/Documents/Code/Python/stacked/Stacked.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class StackedParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NUMBER=1, IDENT=2, WHITESPACE=3, COMMENT=4, MUL=5, DIV=6, ADD=7, SUB=8, 
		POW=9, DUP=10, SWAP=11, DROP=12, OVER=13, ROT=14, POS=15, NEG=16, ZER=17, 
		L_PAREN=18, R_PAREN=19, L_FUNC_PAREN=20, R_FUNC_PAREN=21, FUNC_NAME_SEP=22;
	public static final int
		RULE_start = 0, RULE_value = 1, RULE_statement = 2, RULE_expression = 3, 
		RULE_function_decl = 4, RULE_function_call = 5, RULE_conditional = 6;
	public static final String[] ruleNames = {
		"start", "value", "statement", "expression", "function_decl", "function_call", 
		"conditional"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, "'dup'", "'swap'", 
		"'drop'", "'over'", "'rot'", "'pos'", "'neg'", "'zer'", "'('", "')'", 
		"'{'", "'}'", "'|'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NUMBER", "IDENT", "WHITESPACE", "COMMENT", "MUL", "DIV", "ADD", 
		"SUB", "POW", "DUP", "SWAP", "DROP", "OVER", "ROT", "POS", "NEG", "ZER", 
		"L_PAREN", "R_PAREN", "L_FUNC_PAREN", "R_FUNC_PAREN", "FUNC_NAME_SEP"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Stacked.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public StackedParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(StackedParser.EOF, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(15); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(14);
				statement();
				}
				}
				setState(17); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMBER) | (1L << IDENT) | (1L << MUL) | (1L << DIV) | (1L << ADD) | (1L << SUB) | (1L << POW) | (1L << DUP) | (1L << SWAP) | (1L << DROP) | (1L << OVER) | (1L << ROT) | (1L << POS) | (1L << NEG) | (1L << ZER) | (1L << L_FUNC_PAREN) | (1L << FUNC_NAME_SEP))) != 0) );
			setState(19);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(StackedParser.NUMBER, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_value);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(21);
			match(NUMBER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ConditionalContext conditional() {
			return getRuleContext(ConditionalContext.class,0);
		}
		public Function_declContext function_decl() {
			return getRuleContext(Function_declContext.class,0);
		}
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MUL:
			case DIV:
			case ADD:
			case SUB:
			case POW:
			case DUP:
			case SWAP:
			case DROP:
			case OVER:
			case ROT:
				{
				setState(23);
				expression();
				}
				break;
			case NUMBER:
				{
				setState(24);
				value();
				}
				break;
			case POS:
			case NEG:
			case ZER:
				{
				setState(25);
				conditional();
				}
				break;
			case L_FUNC_PAREN:
			case FUNC_NAME_SEP:
				{
				setState(26);
				function_decl();
				}
				break;
			case IDENT:
				{
				setState(27);
				function_call();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SingleOpContext extends ExpressionContext {
		public Token op;
		public TerminalNode DUP() { return getToken(StackedParser.DUP, 0); }
		public TerminalNode DROP() { return getToken(StackedParser.DROP, 0); }
		public SingleOpContext(ExpressionContext ctx) { copyFrom(ctx); }
	}
	public static class DualOpContext extends ExpressionContext {
		public Token op;
		public TerminalNode MUL() { return getToken(StackedParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(StackedParser.DIV, 0); }
		public TerminalNode ADD() { return getToken(StackedParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(StackedParser.SUB, 0); }
		public TerminalNode POW() { return getToken(StackedParser.POW, 0); }
		public TerminalNode SWAP() { return getToken(StackedParser.SWAP, 0); }
		public TerminalNode OVER() { return getToken(StackedParser.OVER, 0); }
		public DualOpContext(ExpressionContext ctx) { copyFrom(ctx); }
	}
	public static class TripleOpContext extends ExpressionContext {
		public Token op;
		public TerminalNode ROT() { return getToken(StackedParser.ROT, 0); }
		public TripleOpContext(ExpressionContext ctx) { copyFrom(ctx); }
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_expression);
		int _la;
		try {
			setState(33);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DUP:
			case DROP:
				_localctx = new SingleOpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(30);
				((SingleOpContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==DUP || _la==DROP) ) {
					((SingleOpContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case MUL:
			case DIV:
			case ADD:
			case SUB:
			case POW:
			case SWAP:
			case OVER:
				_localctx = new DualOpContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(31);
				((DualOpContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MUL) | (1L << DIV) | (1L << ADD) | (1L << SUB) | (1L << POW) | (1L << SWAP) | (1L << OVER))) != 0)) ) {
					((DualOpContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case ROT:
				_localctx = new TripleOpContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(32);
				((TripleOpContext)_localctx).op = match(ROT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_declContext extends ParserRuleContext {
		public Function_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_decl; }
	 
		public Function_declContext() { }
		public void copyFrom(Function_declContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NamedFunctionContext extends Function_declContext {
		public Token funcname;
		public List<TerminalNode> FUNC_NAME_SEP() { return getTokens(StackedParser.FUNC_NAME_SEP); }
		public TerminalNode FUNC_NAME_SEP(int i) {
			return getToken(StackedParser.FUNC_NAME_SEP, i);
		}
		public TerminalNode L_FUNC_PAREN() { return getToken(StackedParser.L_FUNC_PAREN, 0); }
		public TerminalNode R_FUNC_PAREN() { return getToken(StackedParser.R_FUNC_PAREN, 0); }
		public TerminalNode IDENT() { return getToken(StackedParser.IDENT, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public NamedFunctionContext(Function_declContext ctx) { copyFrom(ctx); }
	}
	public static class AnonFunctionContext extends Function_declContext {
		public TerminalNode L_FUNC_PAREN() { return getToken(StackedParser.L_FUNC_PAREN, 0); }
		public TerminalNode R_FUNC_PAREN() { return getToken(StackedParser.R_FUNC_PAREN, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public AnonFunctionContext(Function_declContext ctx) { copyFrom(ctx); }
	}

	public final Function_declContext function_decl() throws RecognitionException {
		Function_declContext _localctx = new Function_declContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_function_decl);
		try {
			int _alt;
			setState(54);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case L_FUNC_PAREN:
				_localctx = new AnonFunctionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(35);
				match(L_FUNC_PAREN);
				setState(37); 
				_errHandler.sync(this);
				_alt = 1+1;
				do {
					switch (_alt) {
					case 1+1:
						{
						{
						setState(36);
						statement();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(39); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				} while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(41);
				match(R_FUNC_PAREN);
				}
				break;
			case FUNC_NAME_SEP:
				_localctx = new NamedFunctionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(43);
				match(FUNC_NAME_SEP);
				setState(44);
				((NamedFunctionContext)_localctx).funcname = match(IDENT);
				setState(45);
				match(FUNC_NAME_SEP);
				setState(46);
				match(L_FUNC_PAREN);
				setState(48); 
				_errHandler.sync(this);
				_alt = 1+1;
				do {
					switch (_alt) {
					case 1+1:
						{
						{
						setState(47);
						statement();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(50); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
				} while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(52);
				match(R_FUNC_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_callContext extends ParserRuleContext {
		public Function_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call; }
	 
		public Function_callContext() { }
		public void copyFrom(Function_callContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FunctionCallContext extends Function_callContext {
		public Token funcname;
		public TerminalNode IDENT() { return getToken(StackedParser.IDENT, 0); }
		public FunctionCallContext(Function_callContext ctx) { copyFrom(ctx); }
	}

	public final Function_callContext function_call() throws RecognitionException {
		Function_callContext _localctx = new Function_callContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_function_call);
		try {
			_localctx = new FunctionCallContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			((FunctionCallContext)_localctx).funcname = match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalContext extends ParserRuleContext {
		public Token cond;
		public TerminalNode L_PAREN() { return getToken(StackedParser.L_PAREN, 0); }
		public TerminalNode R_PAREN() { return getToken(StackedParser.R_PAREN, 0); }
		public TerminalNode POS() { return getToken(StackedParser.POS, 0); }
		public TerminalNode NEG() { return getToken(StackedParser.NEG, 0); }
		public TerminalNode ZER() { return getToken(StackedParser.ZER, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ConditionalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditional; }
	}

	public final ConditionalContext conditional() throws RecognitionException {
		ConditionalContext _localctx = new ConditionalContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_conditional);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			((ConditionalContext)_localctx).cond = _input.LT(1);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << POS) | (1L << NEG) | (1L << ZER))) != 0)) ) {
				((ConditionalContext)_localctx).cond = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(59);
			match(L_PAREN);
			setState(61); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(60);
				statement();
				}
				}
				setState(63); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMBER) | (1L << IDENT) | (1L << MUL) | (1L << DIV) | (1L << ADD) | (1L << SUB) | (1L << POW) | (1L << DUP) | (1L << SWAP) | (1L << DROP) | (1L << OVER) | (1L << ROT) | (1L << POS) | (1L << NEG) | (1L << ZER) | (1L << L_FUNC_PAREN) | (1L << FUNC_NAME_SEP))) != 0) );
			setState(65);
			match(R_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\30F\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\6\2\22\n\2\r\2\16\2\23"+
		"\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\5\4\37\n\4\3\5\3\5\3\5\5\5$\n\5\3"+
		"\6\3\6\6\6(\n\6\r\6\16\6)\3\6\3\6\3\6\3\6\3\6\3\6\3\6\6\6\63\n\6\r\6\16"+
		"\6\64\3\6\3\6\5\69\n\6\3\7\3\7\3\b\3\b\3\b\6\b@\n\b\r\b\16\bA\3\b\3\b"+
		"\3\b\4)\64\2\t\2\4\6\b\n\f\16\2\5\4\2\f\f\16\16\5\2\7\13\r\r\17\17\3\2"+
		"\21\23\2I\2\21\3\2\2\2\4\27\3\2\2\2\6\36\3\2\2\2\b#\3\2\2\2\n8\3\2\2\2"+
		"\f:\3\2\2\2\16<\3\2\2\2\20\22\5\6\4\2\21\20\3\2\2\2\22\23\3\2\2\2\23\21"+
		"\3\2\2\2\23\24\3\2\2\2\24\25\3\2\2\2\25\26\7\2\2\3\26\3\3\2\2\2\27\30"+
		"\7\3\2\2\30\5\3\2\2\2\31\37\5\b\5\2\32\37\5\4\3\2\33\37\5\16\b\2\34\37"+
		"\5\n\6\2\35\37\5\f\7\2\36\31\3\2\2\2\36\32\3\2\2\2\36\33\3\2\2\2\36\34"+
		"\3\2\2\2\36\35\3\2\2\2\37\7\3\2\2\2 $\t\2\2\2!$\t\3\2\2\"$\7\20\2\2# "+
		"\3\2\2\2#!\3\2\2\2#\"\3\2\2\2$\t\3\2\2\2%\'\7\26\2\2&(\5\6\4\2\'&\3\2"+
		"\2\2()\3\2\2\2)*\3\2\2\2)\'\3\2\2\2*+\3\2\2\2+,\7\27\2\2,9\3\2\2\2-.\7"+
		"\30\2\2./\7\4\2\2/\60\7\30\2\2\60\62\7\26\2\2\61\63\5\6\4\2\62\61\3\2"+
		"\2\2\63\64\3\2\2\2\64\65\3\2\2\2\64\62\3\2\2\2\65\66\3\2\2\2\66\67\7\27"+
		"\2\2\679\3\2\2\28%\3\2\2\28-\3\2\2\29\13\3\2\2\2:;\7\4\2\2;\r\3\2\2\2"+
		"<=\t\4\2\2=?\7\24\2\2>@\5\6\4\2?>\3\2\2\2@A\3\2\2\2A?\3\2\2\2AB\3\2\2"+
		"\2BC\3\2\2\2CD\7\25\2\2D\17\3\2\2\2\t\23\36#)\648A";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}