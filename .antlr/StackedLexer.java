// Generated from /Users/olly/Documents/Code/Python/stacked/Stacked.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class StackedLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NUMBER=1, IDENT=2, WHITESPACE=3, COMMENT=4, MUL=5, DIV=6, ADD=7, SUB=8, 
		POW=9, DUP=10, SWAP=11, DROP=12, OVER=13, ROT=14, POS=15, NEG=16, ZER=17, 
		L_PAREN=18, R_PAREN=19, L_FUNC_PAREN=20, R_FUNC_PAREN=21, FUNC_NAME_SEP=22;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"DIGIT", "CHAR", "NUMBER", "IDENT", "WHITESPACE", "COMMENT", "MUL", "DIV", 
		"ADD", "SUB", "POW", "DUP", "SWAP", "DROP", "OVER", "ROT", "POS", "NEG", 
		"ZER", "L_PAREN", "R_PAREN", "L_FUNC_PAREN", "R_FUNC_PAREN", "FUNC_NAME_SEP"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, "'dup'", "'swap'", 
		"'drop'", "'over'", "'rot'", "'pos'", "'neg'", "'zer'", "'('", "')'", 
		"'{'", "'}'", "'|'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NUMBER", "IDENT", "WHITESPACE", "COMMENT", "MUL", "DIV", "ADD", 
		"SUB", "POW", "DUP", "SWAP", "DROP", "OVER", "ROT", "POS", "NEG", "ZER", 
		"L_PAREN", "R_PAREN", "L_FUNC_PAREN", "R_FUNC_PAREN", "FUNC_NAME_SEP"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public StackedLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Stacked.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\30\u00ae\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\3\2\3\2\3\3\3\3\3\4\6\49\n\4\r\4\16\4:\3\4\3\4\6\4?\n\4\r\4\16\4"+
		"@\5\4C\n\4\3\5\3\5\6\5G\n\5\r\5\16\5H\3\5\3\5\3\6\6\6N\n\6\r\6\16\6O\3"+
		"\6\3\6\3\7\3\7\3\7\3\7\7\7X\n\7\f\7\16\7[\13\7\3\7\3\7\3\7\3\7\3\7\3\b"+
		"\3\b\3\b\3\b\5\bf\n\b\3\t\3\t\3\t\3\t\5\tl\n\t\3\n\3\n\3\n\3\n\5\nr\n"+
		"\n\3\13\3\13\3\13\3\13\5\13x\n\13\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0080\n"+
		"\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3"+
		"\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\23\3"+
		"\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3"+
		"\30\3\31\3\31\3Y\2\32\3\2\5\2\7\3\t\4\13\5\r\6\17\7\21\b\23\t\25\n\27"+
		"\13\31\f\33\r\35\16\37\17!\20#\21%\22\'\23)\24+\25-\26/\27\61\30\3\2\b"+
		"\3\2\62;\4\2C\\c|\4\2..\60\60\5\2\13\f\17\17\"\"\4\2,,zz\4\2\61\61\u00f9"+
		"\u00f9\2\u00b7\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3"+
		"\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2"+
		"\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2"+
		"\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2"+
		"\2\2\3\63\3\2\2\2\5\65\3\2\2\2\78\3\2\2\2\tD\3\2\2\2\13M\3\2\2\2\rS\3"+
		"\2\2\2\17e\3\2\2\2\21k\3\2\2\2\23q\3\2\2\2\25w\3\2\2\2\27\177\3\2\2\2"+
		"\31\u0081\3\2\2\2\33\u0085\3\2\2\2\35\u008a\3\2\2\2\37\u008f\3\2\2\2!"+
		"\u0094\3\2\2\2#\u0098\3\2\2\2%\u009c\3\2\2\2\'\u00a0\3\2\2\2)\u00a4\3"+
		"\2\2\2+\u00a6\3\2\2\2-\u00a8\3\2\2\2/\u00aa\3\2\2\2\61\u00ac\3\2\2\2\63"+
		"\64\t\2\2\2\64\4\3\2\2\2\65\66\t\3\2\2\66\6\3\2\2\2\679\5\3\2\28\67\3"+
		"\2\2\29:\3\2\2\2:8\3\2\2\2:;\3\2\2\2;B\3\2\2\2<>\t\4\2\2=?\5\3\2\2>=\3"+
		"\2\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2AC\3\2\2\2B<\3\2\2\2BC\3\2\2\2C\b"+
		"\3\2\2\2DF\7a\2\2EG\5\5\3\2FE\3\2\2\2GH\3\2\2\2HF\3\2\2\2HI\3\2\2\2IJ"+
		"\3\2\2\2JK\7a\2\2K\n\3\2\2\2LN\t\5\2\2ML\3\2\2\2NO\3\2\2\2OM\3\2\2\2O"+
		"P\3\2\2\2PQ\3\2\2\2QR\b\6\2\2R\f\3\2\2\2ST\7\61\2\2TU\7,\2\2UY\3\2\2\2"+
		"VX\13\2\2\2WV\3\2\2\2X[\3\2\2\2YZ\3\2\2\2YW\3\2\2\2Z\\\3\2\2\2[Y\3\2\2"+
		"\2\\]\7,\2\2]^\7\61\2\2^_\3\2\2\2_`\b\7\2\2`\16\3\2\2\2af\t\6\2\2bc\7"+
		"o\2\2cd\7w\2\2df\7n\2\2ea\3\2\2\2eb\3\2\2\2f\20\3\2\2\2gl\t\7\2\2hi\7"+
		"f\2\2ij\7k\2\2jl\7x\2\2kg\3\2\2\2kh\3\2\2\2l\22\3\2\2\2mr\7-\2\2no\7c"+
		"\2\2op\7f\2\2pr\7f\2\2qm\3\2\2\2qn\3\2\2\2r\24\3\2\2\2sx\7/\2\2tu\7u\2"+
		"\2uv\7w\2\2vx\7d\2\2ws\3\2\2\2wt\3\2\2\2x\26\3\2\2\2y\u0080\7`\2\2z{\7"+
		",\2\2{\u0080\7,\2\2|}\7r\2\2}~\7q\2\2~\u0080\7y\2\2\177y\3\2\2\2\177z"+
		"\3\2\2\2\177|\3\2\2\2\u0080\30\3\2\2\2\u0081\u0082\7f\2\2\u0082\u0083"+
		"\7w\2\2\u0083\u0084\7r\2\2\u0084\32\3\2\2\2\u0085\u0086\7u\2\2\u0086\u0087"+
		"\7y\2\2\u0087\u0088\7c\2\2\u0088\u0089\7r\2\2\u0089\34\3\2\2\2\u008a\u008b"+
		"\7f\2\2\u008b\u008c\7t\2\2\u008c\u008d\7q\2\2\u008d\u008e\7r\2\2\u008e"+
		"\36\3\2\2\2\u008f\u0090\7q\2\2\u0090\u0091\7x\2\2\u0091\u0092\7g\2\2\u0092"+
		"\u0093\7t\2\2\u0093 \3\2\2\2\u0094\u0095\7t\2\2\u0095\u0096\7q\2\2\u0096"+
		"\u0097\7v\2\2\u0097\"\3\2\2\2\u0098\u0099\7r\2\2\u0099\u009a\7q\2\2\u009a"+
		"\u009b\7u\2\2\u009b$\3\2\2\2\u009c\u009d\7p\2\2\u009d\u009e\7g\2\2\u009e"+
		"\u009f\7i\2\2\u009f&\3\2\2\2\u00a0\u00a1\7|\2\2\u00a1\u00a2\7g\2\2\u00a2"+
		"\u00a3\7t\2\2\u00a3(\3\2\2\2\u00a4\u00a5\7*\2\2\u00a5*\3\2\2\2\u00a6\u00a7"+
		"\7+\2\2\u00a7,\3\2\2\2\u00a8\u00a9\7}\2\2\u00a9.\3\2\2\2\u00aa\u00ab\7"+
		"\177\2\2\u00ab\60\3\2\2\2\u00ac\u00ad\7~\2\2\u00ad\62\3\2\2\2\16\2:@B"+
		"HOYekqw\177\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}