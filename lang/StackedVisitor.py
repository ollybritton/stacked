# Generated from Stacked.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .StackedParser import StackedParser
else:
    from StackedParser import StackedParser

# This class defines a complete generic visitor for a parse tree produced by StackedParser.

class StackedVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by StackedParser#start.
    def visitStart(self, ctx:StackedParser.StartContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#value.
    def visitValue(self, ctx:StackedParser.ValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#statement.
    def visitStatement(self, ctx:StackedParser.StatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#SingleOp.
    def visitSingleOp(self, ctx:StackedParser.SingleOpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#DualOp.
    def visitDualOp(self, ctx:StackedParser.DualOpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#TripleOp.
    def visitTripleOp(self, ctx:StackedParser.TripleOpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#AnonFunction.
    def visitAnonFunction(self, ctx:StackedParser.AnonFunctionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#NamedFunction.
    def visitNamedFunction(self, ctx:StackedParser.NamedFunctionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#FunctionCall.
    def visitFunctionCall(self, ctx:StackedParser.FunctionCallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by StackedParser#conditional.
    def visitConditional(self, ctx:StackedParser.ConditionalContext):
        return self.visitChildren(ctx)



del StackedParser