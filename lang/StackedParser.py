# Generated from Stacked.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\30")
        buf.write("F\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\3\2\6\2\22\n\2\r\2\16\2\23\3\2\3\2\3\3\3\3\3\4\3")
        buf.write("\4\3\4\3\4\3\4\5\4\37\n\4\3\5\3\5\3\5\5\5$\n\5\3\6\3\6")
        buf.write("\6\6(\n\6\r\6\16\6)\3\6\3\6\3\6\3\6\3\6\3\6\3\6\6\6\63")
        buf.write("\n\6\r\6\16\6\64\3\6\3\6\5\69\n\6\3\7\3\7\3\b\3\b\3\b")
        buf.write("\6\b@\n\b\r\b\16\bA\3\b\3\b\3\b\4)\64\2\t\2\4\6\b\n\f")
        buf.write("\16\2\5\4\2\f\f\16\16\5\2\7\13\r\r\17\17\3\2\21\23\2I")
        buf.write("\2\21\3\2\2\2\4\27\3\2\2\2\6\36\3\2\2\2\b#\3\2\2\2\n8")
        buf.write("\3\2\2\2\f:\3\2\2\2\16<\3\2\2\2\20\22\5\6\4\2\21\20\3")
        buf.write("\2\2\2\22\23\3\2\2\2\23\21\3\2\2\2\23\24\3\2\2\2\24\25")
        buf.write("\3\2\2\2\25\26\7\2\2\3\26\3\3\2\2\2\27\30\7\3\2\2\30\5")
        buf.write("\3\2\2\2\31\37\5\b\5\2\32\37\5\4\3\2\33\37\5\16\b\2\34")
        buf.write("\37\5\n\6\2\35\37\5\f\7\2\36\31\3\2\2\2\36\32\3\2\2\2")
        buf.write("\36\33\3\2\2\2\36\34\3\2\2\2\36\35\3\2\2\2\37\7\3\2\2")
        buf.write("\2 $\t\2\2\2!$\t\3\2\2\"$\7\20\2\2# \3\2\2\2#!\3\2\2\2")
        buf.write("#\"\3\2\2\2$\t\3\2\2\2%\'\7\26\2\2&(\5\6\4\2\'&\3\2\2")
        buf.write("\2()\3\2\2\2)*\3\2\2\2)\'\3\2\2\2*+\3\2\2\2+,\7\27\2\2")
        buf.write(",9\3\2\2\2-.\7\30\2\2./\7\4\2\2/\60\7\30\2\2\60\62\7\26")
        buf.write("\2\2\61\63\5\6\4\2\62\61\3\2\2\2\63\64\3\2\2\2\64\65\3")
        buf.write("\2\2\2\64\62\3\2\2\2\65\66\3\2\2\2\66\67\7\27\2\2\679")
        buf.write("\3\2\2\28%\3\2\2\28-\3\2\2\29\13\3\2\2\2:;\7\4\2\2;\r")
        buf.write("\3\2\2\2<=\t\4\2\2=?\7\24\2\2>@\5\6\4\2?>\3\2\2\2@A\3")
        buf.write("\2\2\2A?\3\2\2\2AB\3\2\2\2BC\3\2\2\2CD\7\25\2\2D\17\3")
        buf.write("\2\2\2\t\23\36#)\648A")
        return buf.getvalue()


class StackedParser ( Parser ):

    grammarFileName = "Stacked.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'dup'", "'swap'", "'drop'", 
                     "'over'", "'rot'", "'pos'", "'neg'", "'zer'", "'('", 
                     "')'", "'{'", "'}'", "'|'" ]

    symbolicNames = [ "<INVALID>", "NUMBER", "IDENT", "WHITESPACE", "COMMENT", 
                      "MUL", "DIV", "ADD", "SUB", "POW", "DUP", "SWAP", 
                      "DROP", "OVER", "ROT", "POS", "NEG", "ZER", "L_PAREN", 
                      "R_PAREN", "L_FUNC_PAREN", "R_FUNC_PAREN", "FUNC_NAME_SEP" ]

    RULE_start = 0
    RULE_value = 1
    RULE_statement = 2
    RULE_expression = 3
    RULE_function_decl = 4
    RULE_function_call = 5
    RULE_conditional = 6

    ruleNames =  [ "start", "value", "statement", "expression", "function_decl", 
                   "function_call", "conditional" ]

    EOF = Token.EOF
    NUMBER=1
    IDENT=2
    WHITESPACE=3
    COMMENT=4
    MUL=5
    DIV=6
    ADD=7
    SUB=8
    POW=9
    DUP=10
    SWAP=11
    DROP=12
    OVER=13
    ROT=14
    POS=15
    NEG=16
    ZER=17
    L_PAREN=18
    R_PAREN=19
    L_FUNC_PAREN=20
    R_FUNC_PAREN=21
    FUNC_NAME_SEP=22

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class StartContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(StackedParser.EOF, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(StackedParser.StatementContext)
            else:
                return self.getTypedRuleContext(StackedParser.StatementContext,i)


        def getRuleIndex(self):
            return StackedParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = StackedParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_start)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 15 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 14
                self.statement()
                self.state = 17 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << StackedParser.NUMBER) | (1 << StackedParser.IDENT) | (1 << StackedParser.MUL) | (1 << StackedParser.DIV) | (1 << StackedParser.ADD) | (1 << StackedParser.SUB) | (1 << StackedParser.POW) | (1 << StackedParser.DUP) | (1 << StackedParser.SWAP) | (1 << StackedParser.DROP) | (1 << StackedParser.OVER) | (1 << StackedParser.ROT) | (1 << StackedParser.POS) | (1 << StackedParser.NEG) | (1 << StackedParser.ZER) | (1 << StackedParser.L_FUNC_PAREN) | (1 << StackedParser.FUNC_NAME_SEP))) != 0)):
                    break

            self.state = 19
            self.match(StackedParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ValueContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(StackedParser.NUMBER, 0)

        def getRuleIndex(self):
            return StackedParser.RULE_value

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterValue" ):
                listener.enterValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitValue" ):
                listener.exitValue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitValue" ):
                return visitor.visitValue(self)
            else:
                return visitor.visitChildren(self)




    def value(self):

        localctx = StackedParser.ValueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_value)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 21
            self.match(StackedParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(StackedParser.ExpressionContext,0)


        def value(self):
            return self.getTypedRuleContext(StackedParser.ValueContext,0)


        def conditional(self):
            return self.getTypedRuleContext(StackedParser.ConditionalContext,0)


        def function_decl(self):
            return self.getTypedRuleContext(StackedParser.Function_declContext,0)


        def function_call(self):
            return self.getTypedRuleContext(StackedParser.Function_callContext,0)


        def getRuleIndex(self):
            return StackedParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = StackedParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 28
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [StackedParser.MUL, StackedParser.DIV, StackedParser.ADD, StackedParser.SUB, StackedParser.POW, StackedParser.DUP, StackedParser.SWAP, StackedParser.DROP, StackedParser.OVER, StackedParser.ROT]:
                self.state = 23
                self.expression()
                pass
            elif token in [StackedParser.NUMBER]:
                self.state = 24
                self.value()
                pass
            elif token in [StackedParser.POS, StackedParser.NEG, StackedParser.ZER]:
                self.state = 25
                self.conditional()
                pass
            elif token in [StackedParser.L_FUNC_PAREN, StackedParser.FUNC_NAME_SEP]:
                self.state = 26
                self.function_decl()
                pass
            elif token in [StackedParser.IDENT]:
                self.state = 27
                self.function_call()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return StackedParser.RULE_expression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class SingleOpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a StackedParser.ExpressionContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def DUP(self):
            return self.getToken(StackedParser.DUP, 0)
        def DROP(self):
            return self.getToken(StackedParser.DROP, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSingleOp" ):
                listener.enterSingleOp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSingleOp" ):
                listener.exitSingleOp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSingleOp" ):
                return visitor.visitSingleOp(self)
            else:
                return visitor.visitChildren(self)


    class DualOpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a StackedParser.ExpressionContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def MUL(self):
            return self.getToken(StackedParser.MUL, 0)
        def DIV(self):
            return self.getToken(StackedParser.DIV, 0)
        def ADD(self):
            return self.getToken(StackedParser.ADD, 0)
        def SUB(self):
            return self.getToken(StackedParser.SUB, 0)
        def POW(self):
            return self.getToken(StackedParser.POW, 0)
        def SWAP(self):
            return self.getToken(StackedParser.SWAP, 0)
        def OVER(self):
            return self.getToken(StackedParser.OVER, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDualOp" ):
                listener.enterDualOp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDualOp" ):
                listener.exitDualOp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDualOp" ):
                return visitor.visitDualOp(self)
            else:
                return visitor.visitChildren(self)


    class TripleOpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a StackedParser.ExpressionContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def ROT(self):
            return self.getToken(StackedParser.ROT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTripleOp" ):
                listener.enterTripleOp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTripleOp" ):
                listener.exitTripleOp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTripleOp" ):
                return visitor.visitTripleOp(self)
            else:
                return visitor.visitChildren(self)



    def expression(self):

        localctx = StackedParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_expression)
        self._la = 0 # Token type
        try:
            self.state = 33
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [StackedParser.DUP, StackedParser.DROP]:
                localctx = StackedParser.SingleOpContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 30
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==StackedParser.DUP or _la==StackedParser.DROP):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            elif token in [StackedParser.MUL, StackedParser.DIV, StackedParser.ADD, StackedParser.SUB, StackedParser.POW, StackedParser.SWAP, StackedParser.OVER]:
                localctx = StackedParser.DualOpContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 31
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << StackedParser.MUL) | (1 << StackedParser.DIV) | (1 << StackedParser.ADD) | (1 << StackedParser.SUB) | (1 << StackedParser.POW) | (1 << StackedParser.SWAP) | (1 << StackedParser.OVER))) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            elif token in [StackedParser.ROT]:
                localctx = StackedParser.TripleOpContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 32
                localctx.op = self.match(StackedParser.ROT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Function_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return StackedParser.RULE_function_decl

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class NamedFunctionContext(Function_declContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a StackedParser.Function_declContext
            super().__init__(parser)
            self.funcname = None # Token
            self.copyFrom(ctx)

        def FUNC_NAME_SEP(self, i:int=None):
            if i is None:
                return self.getTokens(StackedParser.FUNC_NAME_SEP)
            else:
                return self.getToken(StackedParser.FUNC_NAME_SEP, i)
        def L_FUNC_PAREN(self):
            return self.getToken(StackedParser.L_FUNC_PAREN, 0)
        def R_FUNC_PAREN(self):
            return self.getToken(StackedParser.R_FUNC_PAREN, 0)
        def IDENT(self):
            return self.getToken(StackedParser.IDENT, 0)
        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(StackedParser.StatementContext)
            else:
                return self.getTypedRuleContext(StackedParser.StatementContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNamedFunction" ):
                listener.enterNamedFunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNamedFunction" ):
                listener.exitNamedFunction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNamedFunction" ):
                return visitor.visitNamedFunction(self)
            else:
                return visitor.visitChildren(self)


    class AnonFunctionContext(Function_declContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a StackedParser.Function_declContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def L_FUNC_PAREN(self):
            return self.getToken(StackedParser.L_FUNC_PAREN, 0)
        def R_FUNC_PAREN(self):
            return self.getToken(StackedParser.R_FUNC_PAREN, 0)
        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(StackedParser.StatementContext)
            else:
                return self.getTypedRuleContext(StackedParser.StatementContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnonFunction" ):
                listener.enterAnonFunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnonFunction" ):
                listener.exitAnonFunction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnonFunction" ):
                return visitor.visitAnonFunction(self)
            else:
                return visitor.visitChildren(self)



    def function_decl(self):

        localctx = StackedParser.Function_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_function_decl)
        try:
            self.state = 54
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [StackedParser.L_FUNC_PAREN]:
                localctx = StackedParser.AnonFunctionContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 35
                self.match(StackedParser.L_FUNC_PAREN)
                self.state = 37 
                self._errHandler.sync(self)
                _alt = 1+1
                while _alt!=1 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1+1:
                        self.state = 36
                        self.statement()

                    else:
                        raise NoViableAltException(self)
                    self.state = 39 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,3,self._ctx)

                self.state = 41
                self.match(StackedParser.R_FUNC_PAREN)
                pass
            elif token in [StackedParser.FUNC_NAME_SEP]:
                localctx = StackedParser.NamedFunctionContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 43
                self.match(StackedParser.FUNC_NAME_SEP)
                self.state = 44
                localctx.funcname = self.match(StackedParser.IDENT)
                self.state = 45
                self.match(StackedParser.FUNC_NAME_SEP)
                self.state = 46
                self.match(StackedParser.L_FUNC_PAREN)
                self.state = 48 
                self._errHandler.sync(self)
                _alt = 1+1
                while _alt!=1 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1+1:
                        self.state = 47
                        self.statement()

                    else:
                        raise NoViableAltException(self)
                    self.state = 50 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

                self.state = 52
                self.match(StackedParser.R_FUNC_PAREN)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Function_callContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return StackedParser.RULE_function_call

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class FunctionCallContext(Function_callContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a StackedParser.Function_callContext
            super().__init__(parser)
            self.funcname = None # Token
            self.copyFrom(ctx)

        def IDENT(self):
            return self.getToken(StackedParser.IDENT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionCall" ):
                listener.enterFunctionCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionCall" ):
                listener.exitFunctionCall(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctionCall" ):
                return visitor.visitFunctionCall(self)
            else:
                return visitor.visitChildren(self)



    def function_call(self):

        localctx = StackedParser.Function_callContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_function_call)
        try:
            localctx = StackedParser.FunctionCallContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 56
            localctx.funcname = self.match(StackedParser.IDENT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ConditionalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.cond = None # Token

        def L_PAREN(self):
            return self.getToken(StackedParser.L_PAREN, 0)

        def R_PAREN(self):
            return self.getToken(StackedParser.R_PAREN, 0)

        def POS(self):
            return self.getToken(StackedParser.POS, 0)

        def NEG(self):
            return self.getToken(StackedParser.NEG, 0)

        def ZER(self):
            return self.getToken(StackedParser.ZER, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(StackedParser.StatementContext)
            else:
                return self.getTypedRuleContext(StackedParser.StatementContext,i)


        def getRuleIndex(self):
            return StackedParser.RULE_conditional

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConditional" ):
                listener.enterConditional(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConditional" ):
                listener.exitConditional(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConditional" ):
                return visitor.visitConditional(self)
            else:
                return visitor.visitChildren(self)




    def conditional(self):

        localctx = StackedParser.ConditionalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_conditional)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            localctx.cond = self._input.LT(1)
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << StackedParser.POS) | (1 << StackedParser.NEG) | (1 << StackedParser.ZER))) != 0)):
                localctx.cond = self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 59
            self.match(StackedParser.L_PAREN)
            self.state = 61 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 60
                self.statement()
                self.state = 63 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << StackedParser.NUMBER) | (1 << StackedParser.IDENT) | (1 << StackedParser.MUL) | (1 << StackedParser.DIV) | (1 << StackedParser.ADD) | (1 << StackedParser.SUB) | (1 << StackedParser.POW) | (1 << StackedParser.DUP) | (1 << StackedParser.SWAP) | (1 << StackedParser.DROP) | (1 << StackedParser.OVER) | (1 << StackedParser.ROT) | (1 << StackedParser.POS) | (1 << StackedParser.NEG) | (1 << StackedParser.ZER) | (1 << StackedParser.L_FUNC_PAREN) | (1 << StackedParser.FUNC_NAME_SEP))) != 0)):
                    break

            self.state = 65
            self.match(StackedParser.R_PAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





