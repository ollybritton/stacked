# Generated from Stacked.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .StackedParser import StackedParser
else:
    from StackedParser import StackedParser

# This class defines a complete listener for a parse tree produced by StackedParser.
class StackedListener(ParseTreeListener):

    # Enter a parse tree produced by StackedParser#start.
    def enterStart(self, ctx:StackedParser.StartContext):
        pass

    # Exit a parse tree produced by StackedParser#start.
    def exitStart(self, ctx:StackedParser.StartContext):
        pass


    # Enter a parse tree produced by StackedParser#value.
    def enterValue(self, ctx:StackedParser.ValueContext):
        pass

    # Exit a parse tree produced by StackedParser#value.
    def exitValue(self, ctx:StackedParser.ValueContext):
        pass


    # Enter a parse tree produced by StackedParser#statement.
    def enterStatement(self, ctx:StackedParser.StatementContext):
        pass

    # Exit a parse tree produced by StackedParser#statement.
    def exitStatement(self, ctx:StackedParser.StatementContext):
        pass


    # Enter a parse tree produced by StackedParser#SingleOp.
    def enterSingleOp(self, ctx:StackedParser.SingleOpContext):
        pass

    # Exit a parse tree produced by StackedParser#SingleOp.
    def exitSingleOp(self, ctx:StackedParser.SingleOpContext):
        pass


    # Enter a parse tree produced by StackedParser#DualOp.
    def enterDualOp(self, ctx:StackedParser.DualOpContext):
        pass

    # Exit a parse tree produced by StackedParser#DualOp.
    def exitDualOp(self, ctx:StackedParser.DualOpContext):
        pass


    # Enter a parse tree produced by StackedParser#TripleOp.
    def enterTripleOp(self, ctx:StackedParser.TripleOpContext):
        pass

    # Exit a parse tree produced by StackedParser#TripleOp.
    def exitTripleOp(self, ctx:StackedParser.TripleOpContext):
        pass


    # Enter a parse tree produced by StackedParser#AnonFunction.
    def enterAnonFunction(self, ctx:StackedParser.AnonFunctionContext):
        pass

    # Exit a parse tree produced by StackedParser#AnonFunction.
    def exitAnonFunction(self, ctx:StackedParser.AnonFunctionContext):
        pass


    # Enter a parse tree produced by StackedParser#NamedFunction.
    def enterNamedFunction(self, ctx:StackedParser.NamedFunctionContext):
        pass

    # Exit a parse tree produced by StackedParser#NamedFunction.
    def exitNamedFunction(self, ctx:StackedParser.NamedFunctionContext):
        pass


    # Enter a parse tree produced by StackedParser#FunctionCall.
    def enterFunctionCall(self, ctx:StackedParser.FunctionCallContext):
        pass

    # Exit a parse tree produced by StackedParser#FunctionCall.
    def exitFunctionCall(self, ctx:StackedParser.FunctionCallContext):
        pass


    # Enter a parse tree produced by StackedParser#conditional.
    def enterConditional(self, ctx:StackedParser.ConditionalContext):
        pass

    # Exit a parse tree produced by StackedParser#conditional.
    def exitConditional(self, ctx:StackedParser.ConditionalContext):
        pass


