# Generated from Stacked.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\30")
        buf.write("\u00ae\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\3\2\3\2\3\3\3\3\3\4\6\49\n\4\r\4\16\4:\3\4")
        buf.write("\3\4\6\4?\n\4\r\4\16\4@\5\4C\n\4\3\5\3\5\6\5G\n\5\r\5")
        buf.write("\16\5H\3\5\3\5\3\6\6\6N\n\6\r\6\16\6O\3\6\3\6\3\7\3\7")
        buf.write("\3\7\3\7\7\7X\n\7\f\7\16\7[\13\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\b\3\b\3\b\3\b\5\bf\n\b\3\t\3\t\3\t\3\t\5\tl\n\t\3\n\3")
        buf.write("\n\3\n\3\n\5\nr\n\n\3\13\3\13\3\13\3\13\5\13x\n\13\3\f")
        buf.write("\3\f\3\f\3\f\3\f\3\f\5\f\u0080\n\f\3\r\3\r\3\r\3\r\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22")
        buf.write("\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\26")
        buf.write("\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3Y\2\32\3\2\5\2\7")
        buf.write("\3\t\4\13\5\r\6\17\7\21\b\23\t\25\n\27\13\31\f\33\r\35")
        buf.write("\16\37\17!\20#\21%\22\'\23)\24+\25-\26/\27\61\30\3\2\b")
        buf.write("\3\2\62;\4\2C\\c|\4\2..\60\60\5\2\13\f\17\17\"\"\4\2,")
        buf.write(",zz\4\2\61\61\u00f9\u00f9\2\u00b7\2\7\3\2\2\2\2\t\3\2")
        buf.write("\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2")
        buf.write("\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2")
        buf.write("\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#")
        buf.write("\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2")
        buf.write("\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\3\63\3\2\2\2\5\65")
        buf.write("\3\2\2\2\78\3\2\2\2\tD\3\2\2\2\13M\3\2\2\2\rS\3\2\2\2")
        buf.write("\17e\3\2\2\2\21k\3\2\2\2\23q\3\2\2\2\25w\3\2\2\2\27\177")
        buf.write("\3\2\2\2\31\u0081\3\2\2\2\33\u0085\3\2\2\2\35\u008a\3")
        buf.write("\2\2\2\37\u008f\3\2\2\2!\u0094\3\2\2\2#\u0098\3\2\2\2")
        buf.write("%\u009c\3\2\2\2\'\u00a0\3\2\2\2)\u00a4\3\2\2\2+\u00a6")
        buf.write("\3\2\2\2-\u00a8\3\2\2\2/\u00aa\3\2\2\2\61\u00ac\3\2\2")
        buf.write("\2\63\64\t\2\2\2\64\4\3\2\2\2\65\66\t\3\2\2\66\6\3\2\2")
        buf.write("\2\679\5\3\2\28\67\3\2\2\29:\3\2\2\2:8\3\2\2\2:;\3\2\2")
        buf.write("\2;B\3\2\2\2<>\t\4\2\2=?\5\3\2\2>=\3\2\2\2?@\3\2\2\2@")
        buf.write(">\3\2\2\2@A\3\2\2\2AC\3\2\2\2B<\3\2\2\2BC\3\2\2\2C\b\3")
        buf.write("\2\2\2DF\7a\2\2EG\5\5\3\2FE\3\2\2\2GH\3\2\2\2HF\3\2\2")
        buf.write("\2HI\3\2\2\2IJ\3\2\2\2JK\7a\2\2K\n\3\2\2\2LN\t\5\2\2M")
        buf.write("L\3\2\2\2NO\3\2\2\2OM\3\2\2\2OP\3\2\2\2PQ\3\2\2\2QR\b")
        buf.write("\6\2\2R\f\3\2\2\2ST\7\61\2\2TU\7,\2\2UY\3\2\2\2VX\13\2")
        buf.write("\2\2WV\3\2\2\2X[\3\2\2\2YZ\3\2\2\2YW\3\2\2\2Z\\\3\2\2")
        buf.write("\2[Y\3\2\2\2\\]\7,\2\2]^\7\61\2\2^_\3\2\2\2_`\b\7\2\2")
        buf.write("`\16\3\2\2\2af\t\6\2\2bc\7o\2\2cd\7w\2\2df\7n\2\2ea\3")
        buf.write("\2\2\2eb\3\2\2\2f\20\3\2\2\2gl\t\7\2\2hi\7f\2\2ij\7k\2")
        buf.write("\2jl\7x\2\2kg\3\2\2\2kh\3\2\2\2l\22\3\2\2\2mr\7-\2\2n")
        buf.write("o\7c\2\2op\7f\2\2pr\7f\2\2qm\3\2\2\2qn\3\2\2\2r\24\3\2")
        buf.write("\2\2sx\7/\2\2tu\7u\2\2uv\7w\2\2vx\7d\2\2ws\3\2\2\2wt\3")
        buf.write("\2\2\2x\26\3\2\2\2y\u0080\7`\2\2z{\7,\2\2{\u0080\7,\2")
        buf.write("\2|}\7r\2\2}~\7q\2\2~\u0080\7y\2\2\177y\3\2\2\2\177z\3")
        buf.write("\2\2\2\177|\3\2\2\2\u0080\30\3\2\2\2\u0081\u0082\7f\2")
        buf.write("\2\u0082\u0083\7w\2\2\u0083\u0084\7r\2\2\u0084\32\3\2")
        buf.write("\2\2\u0085\u0086\7u\2\2\u0086\u0087\7y\2\2\u0087\u0088")
        buf.write("\7c\2\2\u0088\u0089\7r\2\2\u0089\34\3\2\2\2\u008a\u008b")
        buf.write("\7f\2\2\u008b\u008c\7t\2\2\u008c\u008d\7q\2\2\u008d\u008e")
        buf.write("\7r\2\2\u008e\36\3\2\2\2\u008f\u0090\7q\2\2\u0090\u0091")
        buf.write("\7x\2\2\u0091\u0092\7g\2\2\u0092\u0093\7t\2\2\u0093 \3")
        buf.write("\2\2\2\u0094\u0095\7t\2\2\u0095\u0096\7q\2\2\u0096\u0097")
        buf.write("\7v\2\2\u0097\"\3\2\2\2\u0098\u0099\7r\2\2\u0099\u009a")
        buf.write("\7q\2\2\u009a\u009b\7u\2\2\u009b$\3\2\2\2\u009c\u009d")
        buf.write("\7p\2\2\u009d\u009e\7g\2\2\u009e\u009f\7i\2\2\u009f&\3")
        buf.write("\2\2\2\u00a0\u00a1\7|\2\2\u00a1\u00a2\7g\2\2\u00a2\u00a3")
        buf.write("\7t\2\2\u00a3(\3\2\2\2\u00a4\u00a5\7*\2\2\u00a5*\3\2\2")
        buf.write("\2\u00a6\u00a7\7+\2\2\u00a7,\3\2\2\2\u00a8\u00a9\7}\2")
        buf.write("\2\u00a9.\3\2\2\2\u00aa\u00ab\7\177\2\2\u00ab\60\3\2\2")
        buf.write("\2\u00ac\u00ad\7~\2\2\u00ad\62\3\2\2\2\16\2:@BHOYekqw")
        buf.write("\177\3\b\2\2")
        return buf.getvalue()


class StackedLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    NUMBER = 1
    IDENT = 2
    WHITESPACE = 3
    COMMENT = 4
    MUL = 5
    DIV = 6
    ADD = 7
    SUB = 8
    POW = 9
    DUP = 10
    SWAP = 11
    DROP = 12
    OVER = 13
    ROT = 14
    POS = 15
    NEG = 16
    ZER = 17
    L_PAREN = 18
    R_PAREN = 19
    L_FUNC_PAREN = 20
    R_FUNC_PAREN = 21
    FUNC_NAME_SEP = 22

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'dup'", "'swap'", "'drop'", "'over'", "'rot'", "'pos'", "'neg'", 
            "'zer'", "'('", "')'", "'{'", "'}'", "'|'" ]

    symbolicNames = [ "<INVALID>",
            "NUMBER", "IDENT", "WHITESPACE", "COMMENT", "MUL", "DIV", "ADD", 
            "SUB", "POW", "DUP", "SWAP", "DROP", "OVER", "ROT", "POS", "NEG", 
            "ZER", "L_PAREN", "R_PAREN", "L_FUNC_PAREN", "R_FUNC_PAREN", 
            "FUNC_NAME_SEP" ]

    ruleNames = [ "DIGIT", "CHAR", "NUMBER", "IDENT", "WHITESPACE", "COMMENT", 
                  "MUL", "DIV", "ADD", "SUB", "POW", "DUP", "SWAP", "DROP", 
                  "OVER", "ROT", "POS", "NEG", "ZER", "L_PAREN", "R_PAREN", 
                  "L_FUNC_PAREN", "R_FUNC_PAREN", "FUNC_NAME_SEP" ]

    grammarFileName = "Stacked.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


