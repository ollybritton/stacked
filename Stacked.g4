grammar Stacked;

// Tokens
fragment DIGIT: [0-9];
fragment CHAR: [a-zA-Z];

NUMBER: DIGIT+ ([.,] DIGIT+)?;
IDENT: '_' CHAR+ '_';

WHITESPACE: [ \r\n\t]+ -> skip;
COMMENT: '/*' .*? '*/' -> skip;

MUL: '*' | 'x' | 'mul';
DIV: '/' | '÷' | 'div';
ADD: '+' | 'add';
SUB: '-' | 'sub';
POW: '^' | '**' | 'pow';
DUP: 'dup';
SWAP: 'swap';
DROP: 'drop';
OVER: 'over';
ROT: 'rot';

POS: 'pos';
NEG: 'neg';
ZER: 'zer';

L_PAREN: '(';
R_PAREN: ')';

L_FUNC_PAREN: '{';
R_FUNC_PAREN: '}';

FUNC_NAME_SEP: '|';

// Parser Rules
start: statement+ EOF;
value: NUMBER;
statement: (expression|value|conditional|function_decl|function_call);

expression
   : op=(DUP|DROP)                      # SingleOp // An operation that operates on one stack value.
   | op=(MUL|DIV|ADD|SUB|POW|SWAP|OVER) # DualOp   // An operation that operates on two values on the stack.
   | op=ROT                             # TripleOp // An operation that operates on three values on the stack.
   ;

function_decl
   : L_FUNC_PAREN statement+? R_FUNC_PAREN # AnonFunction
   | FUNC_NAME_SEP funcname=IDENT FUNC_NAME_SEP L_FUNC_PAREN statement+? R_FUNC_PAREN # NamedFunction
   ;

function_call
   : funcname=IDENT # FunctionCall;

conditional
   : cond=(POS|NEG|ZER) L_PAREN statement+ R_PAREN
   ;