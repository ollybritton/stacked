import os
import sys
import json
import antlr4

import lang

class Stack():
    def __init__(self):
        self.stack = []

    def pop(self):
        return self.stack.pop(0)

    def push(self, val):
        self.stack = [val] + self.stack

class Visitor(lang.StackedVisitor):
    def __init__(self):
        self.fList = {}
        self.stack = Stack()

        self.functions = {}

    def visitValue(self, ctx):
        # This is called when a value is added to the stack. For example:
        # 1 1 1 1 => [1 1 1 1]

        value = float(ctx.getText())
        self.stack.push(value)


    # Operations on the stack
    def visitSingleOp(self, ctx):
        # This is called whenever there is an operator that operates on the top item of the stack only.
        # [dup, drop] are both examples of these.

        top = self.stack.pop()

        if ctx.DUP():
            self.stack.push(top)
            self.stack.push(top)

        elif ctx.DROP():
            pass

        else:
            raise Exception(f"Unknown dual operator {ctx.getString()}")

    def visitDualOp(self, ctx):
        # This is called whenever there is an operator that operates on the top two items of the stack.
        # [*, /, +, -, ^, swp, over] are all examples of these

        right, left = self.stack.pop(), self.stack.pop()

        if ctx.MUL():
            self.stack.push(left*right)

        elif ctx.DIV():
            self.stack.push(left/right)

        elif ctx.ADD():
            self.stack.push(left+right)

        elif ctx.SUB():
            self.stack.push(left-right)

        elif ctx.POW():
            self.stack.push(left**right)

        elif ctx.SWAP():
            self.stack.push(right)
            self.stack.push(left)

        elif ctx.OVER():
            self.stack.push(left)
            self.stack.push(right)
            self.stack.push(left)

        else:
            raise Exception(f"Unknown dual operator {ctx.getString()}")
        
    def visitTripleOp(self, ctx):
        # This is called whenever there is an operator that operates on the top three items of the stack.
        # [rot] is an example of this.

        three, two, one = self.stack.pop(), self.stack.pop(), self.stack.pop()

        if ctx.ROT():
            self.stack.push(one)
            self.stack.push(three)
            self.stack.push(two)

        else:
            raise Exception(f"Unknown triple operator {ctx.getString()}")

    # Conditionals
    def visitConditional(self, ctx):
        # A conditional adds an if-like functionality to the language. It is kind of like assembly with the branch functions.

        # pos(val) - Only runs val to the stack if the top of the stack is positive.
        # zer(val) - Only runs val to the stack if the top of the stack is zero.
        # neg(val) - Only runs val to the stack if the top of the stack is negative.
        
        top = self.stack.pop()
        self.stack.push(top)

        if ctx.POS() and top <= 0:
            return

        if ctx.NEG() and top >= 0:
            return

        if ctx.ZER() and top != 0:
            return

        for statement in ctx.statement():
            self.visit(statement)
                
    # Functions
    def visitAnonFunction(self, ctx):
        for statement in ctx.statement():
            self.visit(statement)


    def visitNamedFunction(self, ctx):
        function_name = ctx.funcname.text
        self.functions[function_name] = ctx.statement()


    def visitFunctionCall(self, ctx):
        function_name = ctx.funcname.text
        statements = self.functions.get(function_name)

        if statements:
            for statement in statements:
                self.visit(statement)

        else:
            raise Exception(f"Undefined function '{function_name}'")


    


def main():
    file = lang.FileStream(sys.argv[1])
    lexer = lang.StackedLexer(file)
    stream = lang.CommonTokenStream(lexer)
    parser = lang.StackedParser(stream)

    tree = parser.start()

    if parser.getNumberOfSyntaxErrors()!=0:
        print("File contains {} "
              "syntax errors".format(parser.getNumberOfSyntaxErrors()))
        return
 
    visitor = Visitor()
    visitor.fList = {}
    visitor.visit(tree)

    print("Final Stack:", visitor.stack.stack)



if __name__ == "__main__":
    main()